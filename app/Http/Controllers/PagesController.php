<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Post;

class PagesController extends Controller
{
    public function getIndex() {
        $posts = Post::orderBy('created_at', 'desc')->limit(4)->get();

        return view('pages.welcome')->withPosts($posts);
    }
    
    public function getAbout() {
        $first = 'Alex';
        $last = 'Curtis';

        $fullname = $first . ' ' . $last;
        $email = 'alex@curtis.com';

        $data = [];
        $data['email'] = $email;
        $data['fullname'] = $fullname;

        return view('pages.about')/*->with('fullname', $full)*/ // method ini -> view = {{ $fullname }}
                                /*->withFullname($fullname)*/ // method sama dg diatas (F harus capital)
                                /*->withEmail($email)*/
                                ->withData($data) // method dg array -> view = {{ $data['fullname' / 'email']}}
            ;
    }

    public function getContact() {
        return view('pages.contact');
    }
}
