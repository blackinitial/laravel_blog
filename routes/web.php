<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Route::group(['middleware' => ['web']], function() {

	Route::get('blog/{slug}', ['as' => 'blog.single', 'uses' => 'BlogController@getSingle'])->where('slug', '[\w\d\-\_]+'); //where slug = supaya tidak bisa di passing lwt url
	Route::get('blog', ['uses' => 'BlogController@getArchive', 'as' => 'blog.index']);

	Route::get('/', 'PagesController@getIndex');
	Route::get('/about', 'PagesController@getAbout');
	Route::get('/contact', 'PagesController@getContact');

	Route::resource('posts', 'PostController'); //resource tanpa url route, hidden url $ sudah as name (route:list) 

});