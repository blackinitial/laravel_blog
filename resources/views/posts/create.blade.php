@extends('main')

@section('title', '| Create New Post')

@section('stylesheets')
	
	{!! Html::style('css/parsley.css') !!} //link css

@stop

@section('content')

	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<h1>Create New Post</h1>
			<hr>
			
			{!! Form::open(array('route' => 'posts.store')) !!} <!-- must add , 'data-parsley-validate' => '' validate js if not laravel session-->
    			{{ Form::label('title', 'Title:') }}
    			{{ Form::text('title', null, array('class' => 'form-control')) }} <!-- validate required , 'required' => '' if with js not laravel session-->
				
				{{ Form::label('slug', 'Slug:') }}
    			{{ Form::text('slug', null, array('class' => 'form-control', 'required' => '', 'minlength' => '5', 'maxlength' => '255' )) }}

    			{{ Form::label('body', 'Post Body:') }}
    			{{ Form::textarea('body', null, array('class' => 'form-control')) }} <!-- validate required , 'required' => '' if with js not laravel session-->

    			{{ Form::submit('Create Post', array('class' => 'btn btn-success btn-lg btn-block', 'style' => 'margin-top: 20px;')) }}
			{!! Form::close() !!}

		</div>
	</div>

@stop

@section('scripts')
	
	{!! Html::script('js/parsley.min.js') !!} <!-- link to add javascript validate -->

@stop