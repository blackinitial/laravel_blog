<!DOCTYPE html>
<html lang="en">
    
<head>
@include('partials._head')
</head>

    <body>

@include('partials._nav')

    <!-- Container -->
    <div class="container">

		@include('partials._message') <!-- add validate session flash message laravel -->

		@yield('content')

		@include('partials._footer')

    </div>
    <!-- /Container -->

@include('partials._javascript')

@yield('scripts')

    </body>
</html>